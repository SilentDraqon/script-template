################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel


################################################################################
#   Main Panel
################################################################################

class MainPanel(Panel):
    bl_idname = "OBJECT_PT_PANEL_NEW_SCRIPT_TEMPLATE"
    bl_label = "New Script"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context is not None

    def draw(self, context):
        panel = self.layout
        row = panel.row()
        row.label(text="Hellouw World")
        return
