################################################################################
#   Package Information
################################################################################

bl_info = {
    "name": "new-script",
    "blender": (2, 92, 0),
    "category": "User Interface",
    "version": (0, 1, 0, 0),
    "location": "VIEW_3D",
}


################################################################################
#   Imported Libraries
################################################################################

# Reload Addon Operators
if "bpy" in locals():
    import importlib
    importlib.reload(MainPanel)

# Import Addon Operators
else:
    from .panels.mainpanel import MainPanel

# Internal Libraries
import bpy
from bpy.props import EnumProperty, StringProperty, BoolProperty, IntProperty, FloatProperty
from bpy.types import Scene
from bpy.utils import register_class, unregister_class


################################################################################
#   Registration
################################################################################

classes = [
    MainPanel
]

def register():

    # Register Classes
    for operator in classes:
        register_class(operator)

    # Register Properties
    # ...

def unregister():
    
    # Unregister Classes
    for operator in classes:
        unregister_class(operator)

    # Unregister Properties
    # ...


if __name__ == "__main__":
    register()
